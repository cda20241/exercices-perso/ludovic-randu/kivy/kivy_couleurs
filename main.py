from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput

rouge = {"couleur": "Rouge", "code": [0.98, 0.173, 0.173, 1.0]}
bleu = {"couleur": "Bleu", "code": [0, 0, 1, 1]}
vert = {"couleur": "Vert", "code": [0.357, 1, 0.529, 1.0]}
jaune = {"couleur": "Jaune", "code": [0.992, 0.965, 0.137, 1.0]}
rose = {"couleur": "Rose", "code": [0.98, 0.173, 0.953, 1.0]}
marron = {"couleur": "Marron", "code": [0.8, 0.4, 0, 1.0]}
colors = [rouge, bleu, vert, jaune, rose, marron]


class Couleurs(App):
    Input1 = None
    Label1 = None
    Liste_Boutons = None
    layoutBouton = None
    layout = None

    def build(self):
        self.layout = GridLayout(cols=1)
        self.layoutBouton = GridLayout(cols=3)
        self.Label()
        self.Input()
        self.Boutons()
        self.layout.add_widget(self.layoutBouton)
        return self.layout

    def Boutons(self):
        self.Liste_Boutons = []
        for i in range(0, len(colors)):
            self.Liste_Boutons.append(Button(text=colors[i]["couleur"], size_hint_y=None, width=100))
            self.Liste_Boutons[i].background_normal = ""
            self.Liste_Boutons[i].background_color = colors[i]["code"]
            self.Liste_Boutons[i].bind(on_press=self.changeCouleurLabel)
            # On ajoute le bouton au layout principal:
            self.layoutBouton.add_widget(self.Liste_Boutons[i])

    def Label(self):
        # On cree un label avec toutes ses proprietes:
        self.Label1 = Label(text="Entrez du texte", font_size=30, color=[1, 1, 1, 1])
        # On l'ajoute au layout principal:
        self.layout.add_widget(self.Label1)

    def Input(self):
        # On cree un Input avec des ses proprietes:
        self.Input1 = TextInput(font_size=20, size_hint=(.1, None), height=50)
        # Fonction appelée chaque fois que le texte dans TextInput change
        self.Input1.bind(text=self.on_text_change)
        # On l'ajoute au layout principal:
        self.layout.add_widget(self.Input1)

    def changeCouleurLabel(self, instance):
        # Change la couleur du input lorsqu'on appuye sur un bouton
        self.Label1.color = instance.background_color

    def on_text_change(self, instance, value):
        # Cette fonction est appelée chaque fois que le texte dans TextInput change
        self.Label1.text = value


if __name__ == '__main__':
    Couleurs().run()
